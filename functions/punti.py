from common_imports import *

@checkRoles(min_role = 0)
def punti(command, people, id_map):
    u"""/punti\n\nRestituisce la quantita' di punti della persona che lo usa."""
    bot.sendMessage(command.chat_id, people[command.fid]['sname']+', i tuoi punti ammontano a: '+ str(people[command.fid]['punti']))
    return 0
