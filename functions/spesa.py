# coding=utf-8%
from common_imports import *    

@checkRoles(min_role = 0)
def spesa(command, people, id_map):
    u"""/spesa <importo>\n\nDa usare per riavere <importo>, speso per il bene della saletta"""
    try:
        if check_nan(command, people, id_map, 1):
            return 1
        if float(command.message.split()[1])>=0:
            c=abs(float(command.message.split()[1]))
        else:
            bot.sendMessage(command.chat_id, 'Importi negativi non consentiti.')
            return 1
    except:
        bot.sendMessage(command.chat_id, 'Importo selezionato non valido!\nDigita /spesa <importo> per avere quanto ti spetta per la spesa!')
        return 1
    people[command.fid]['saldo']+=c
    bot.sendMessage(command.chat_id, people[command.fid]['sname'] + ' ha ricevuto '+str(c)+u' \u20AC  per aver fatto la spesa! Saldo: '+str(people[command.fid]['saldo'])+u'\u20AC.')
    return 0
