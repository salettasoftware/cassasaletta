# coding=utf-8%                              
from common_imports import *

@checkRoles(min_role = 0)
def saldo(command, people, id_map):
    u"""/saldo\n\nComunica il saldo attuale personale."""
    bot.sendMessage(command.chat_id, 'Il saldo di '+people[command.fid]['sname']+u'  è '+str(people[command.fid]['saldo'])+u'\u20AC.')
    return 0
