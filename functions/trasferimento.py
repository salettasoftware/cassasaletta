from common_imports import *

@checkRoles(min_role = 0)
def trasferimento(command, people, id_map):
    u"""Uso standard:\n/trasferimento <importo> <NomeSaletta>\n\ntrasferisce <importo> da chi chiama la funzione a <NomeSaletta>\n\n Per chi ha i privilegi necessari:\n/trasferimentoP <importo> <debitore> <creditore>\n\ntrasferisce <importo> da <debitore> a <creditore."""
    if check_nan(command, people, id_map, 1):
        return 1
    try:
        if float(command.message.split()[1])>=0:
            t=abs(float(command.message.split()[1]))
        else:
            bot.sendMessage(command.chat_id, 'Importi negativi non consentiti.')
            return 1
        if command.fid != return_fid(command, people, id_map, command.message.split()[2]):
            recharger=command.fid
            to_be_recharged=return_fid(command, people, id_map, command.message.split()[2])
        else:
            bot.sendMessage(command.chat_id, 'A paraculo/a..')
            return 1
        people[to_be_recharged]['saldo']+=t
        people[recharger]['saldo']-=t
            
        bot.sendMessage(command.chat_id, 'Trasferimento avvenuto con successo.\nIl saldo di '+people[recharger]['sname']+' passa da '+str(people[recharger]['saldo']+t)+u'\u20AC a '+str(people[recharger]['saldo'])+u'\u20AC.\nQuello di '+people[to_be_recharged]['sname']+' passa da '+str(people[to_be_recharged]['saldo']-t)+u'\u20AC a '+str(people[to_be_recharged]['saldo'])+u'\u20AC.')
        return 0
    except:
        bot.sendMessage(command.chat_id, 'Mmmm '+people[command.fid]['sname']+', secondo me hai toppato qualcosa coi nomi.')
        return 1
