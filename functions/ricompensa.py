# coding=utf-8%
from common_imports import *

@checkRoles(min_role = 2)
def ricompensa(command, people, id_map):
    u"""/ricompensa <nome saletta> <quantità>\n\nRicompensa <nome saletta> con <quantità> di punti per i servigi da lui svolti. Sono necessari privilegi particolari per usare questa funzione."""

    try:
        person=id_map[command.message.split()[1]]['id']
    except:
        bot.sendMessage(command.chat_id, 'Errore, nome scorretto o non esistente.')
        return 1
    
    ricompensa=float(command.message.split()[2])
    people[person]['punti']+=ricompensa
    bot.sendMessage(command.chat_id, 'Complimenti '+ people[person]['sname']+', hai ricevuto '+str(ricompensa)+' punti come ricompensa per i tuoi servigi.')
    return 0
