# coding=utf-8%                              
from common_imports import *

@checkRoles(min_role = 0)
def sticker(command, people, id_map, riscatta_flag=False):
    u"""/sticker <n> \n\npaga <n> sticker di prima generazione unici ed inimitabili. (<n> = 1 se omesso)"""
    n_sticker = 1
    if len(command.message.split()) > 1:
        try:
            if check_nan(command, people, id_map, 1):
                return 1
            else:
                n_sticker = int(command.message.split()[1])
        except:
            bot.sendMessage(command.chat_id, "Il numero di sticker che vuoi deve essere un numero, tacchino.")
            return 1
    if not riscatta_flag:
        people[command.fid]['saldo']-=people[command.fid]['ban_score'][0]*0.50*n_sticker
        people[command.fid]['punti']+=0.5*n_sticker
        str_message='Grande '+str(people[command.fid]['sname'])+u', hai pagato per '+str(n_sticker)+' sticker! Il tuo saldo è adesso pari a '+str(people[command.fid]['saldo'])+u'\u20AC.'
    if riscatta_flag:
        str_message='Grande '+str(people[command.fid]['sname'])+u', sticker riscattato! Il tuo saldo rimane a '+str(people[command.fid]['saldo'])+u'\u20AC.'
    bot.sendMessage(command.chat_id, str_message)
    return 0
