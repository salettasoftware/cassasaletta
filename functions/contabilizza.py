# coding=utf-8%
from common_imports import *
from pasta import pasta
from caffe import caffe
from paga import paga

@checkRoles(min_role = 0)
def contabilizza(command, people, id_map):
    u"""/contabilizza <causale> <importo> <persone>\n\nContabilizza a tutte le <persone>(scritte separate da uno spazio) un item specificato in <causale>, del costo di <importo>"""
    people_eating=command.message.split()[2:]
    try:
        if command.message.split()[1]!='caffe':
            if float(command.message.split()[2])<0:
                bot.sendMessage(command.chat_id, 'Importi negativi non consentiti.')
                return 1
    except:
        bot.sendMessage(command.chat_id, u"Mmmm, c'è qualcosa che è andato storto. (Sintassi: /contabilizza <causale> <importo> <persone>)")
        return 1
    
    if command.message.split()[1]=='pasta':
        try:
            if check_nan(command, people, id_map, 2):
                bot.sendMessage(command.chat_id, 'Importo non valido')
                return 1
        except:
            bot.sendMessage(command.chat_id, 'Importo non valido')
            return 1
        try:
            people_eating=command.message.split()[3:]
            temp_message = command.message
            command.set_message('/pasta'+' '+command.message.split()[2])
            for person in set(people_eating):
                command.set_fid(return_fid(command, people, id_map, person))
                out=pasta(command, people, id_map)
                if out==1:
                    return 1
            command.set_message(temp_message)
            return 0
        except:
            bot.sendMessage(command.chat_id, u"Mmmm, c'è qualcosa che è andato storto. (Sintassi: /contabilizza <causale> <importo> <persone>)")
            return 1
    elif command.message.split()[1]=='caffe':
        try:
            people_eating=command.message.split()[2:]
            for person in set(people_eating):
                command.set_fid(return_fid(command, people, id_map, person))
                caffe(command, people, id_map)
                return 0
        except:
            bot.sendMessage(command.chat_id, u"Mmmm, c'è qualcosa che è andato storto. (Sintassi: /contabilizza <causale> <importo> <persone>)")
            return 1
        
    else:
        try:
            if check_nan(command, people, id_map, 2):
                return 1
        except:
            bot.sendMessage(command.chat_id, 'Importo non valido')
            return 1
        try:
            people_eating=command.message.split()[3:]            
            print('people:' ,people_eating)
            temp_message = command.message
            command.set_message('/paga '+command.message.split()[2]+' '+command.message.split()[1])
            for person in set(people_eating):
                command.set_fid(return_fid(command, people, id_map, person))
                print(command.message, command.fid)
                paga(command, people, id_map, disable_message=True)
            command.set_message(temp_message)
            return 0
        except:
            bot.sendMessage(command.chat_id, u"Mmmm, c'è qualcosa che è andato storto. (Sintassi: /contabilizza <causale> <importo> <persone>)")
            return 1
