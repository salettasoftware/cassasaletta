from common_imports import *

@checkRoles(min_role = 0)
def preleva(command, people, id_map):
    u"""/preleva <importo>\n\nDa usare in caso si desideri prelevare in contanti <importo> dalle casse della saletta."""
    try:
        if check_nan(command, people, id_map, 1):
            return 1
        if float(command.message.split()[1])>=0:
            c=abs(float(command.message.split()[1]))
        else:
            bot.sendMessage(command.chat_id, 'Importi negativi non consentiti.')
            return 1
    except:
        bot.sendMessage(command.chat_id, 'Importo selezionato non valido!\nDigita /preleva <importo> per contabilizzare il prelievo!')
        return 1
    if (people['CassaSaletta']['saldo']-c)>=0:
        people[command.fid]['saldo']-=c
        people['CassaSaletta']['saldo']-=c
        bot.sendMessage(command.chat_id, people[command.fid]['sname'] + ' ha prelevato '+str(c)+u'\u20AC! Saldo rimanente: '+str(people[command.fid]['saldo'])+u'\u20AC.')
        return 0
    else:
        bot.sendMessage(command.chat_id, 'Operazione fallita. La saletta non ha abbastanza fondi per supportare un prelievo di '+str(c)+u'\u20AC.')
        return 1
