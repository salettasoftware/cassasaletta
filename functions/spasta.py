from common_imports import *

@checkRoles(min_role = 0)
def spasta(command, people, id_map, disable_message=False):
    u"""/spasta <importo>\n\nAumenta il saldo personale di <importo> ed elimina una pasta, senza toccare il saldo della cassa, a differenza di ricarica. E' possibile annullare una pasta solo il giorno stesso in cui e' stata pagata."""
    if check_nan(command, people, id_map, 1):
        return 0
    try:
        if float(command.message.split()[1])>=0:
            c=abs(float(command.message.split()[1]))
        else:
            bot.sendMessage(command.chat_id, 'Importi negativi non consentiti.')
            return 1
        
    except:
        if not disable_message:
            bot.sendMessage(command.chat_id, 'Importo selezionato non valido!\nDigita /spaga <importo> per rimediare ai tuoi errori!')
            return 1
    costo=people[command.fid]['ban_score'][0]*c
    spaga_flag=1
    for i in range(1, min(11, len(people[command.fid]['pasta']))):
        if people[command.fid]['pasta'][-i][1].day==command.date.day and people[command.fid]['pasta'][-i][1].month==command.date.month:
            if people[command.fid]['pasta'][-i][0]==costo:
                people[command.fid]['saldo']+=costo
                people[command.fid]['punti']-=c
                people[command.fid]['pasta'].pop(-i)
                bot.sendMessage(command.chat_id, people[command.fid]['sname'] + ' ha annullato il pagamento di  '+str(costo)+u'\u20AC! Il saldo torna a: '+str(people[command.fid]['saldo'])+u'\u20AC.')
                spaga_flag=0
                return 0
    
    if spaga_flag:
        bot.sendMessage(command.chat_id, people[command.fid]['sname'] + ' fra le ultime dieci paste pare non ce ne sia nessuna che corrisponda a '+str(costo)+u"\u20AC. Stai provando a rubacchiare per caso? Il ban e' dietro l'angolo.")
