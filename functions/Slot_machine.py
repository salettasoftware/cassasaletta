import random

def nurandint():
    n=random.random()
    lowP=0.38
    nlowP=6
    ntot=10
    lowPbin=lowP/nlowP
    highPbin=(1-lowP)/(ntot-nlowP)
    if n<=lowP:
        for i in range(nlowP):
            if n>i*lowPbin and n<=(i+1)*lowPbin:
                return i
    else:
        for i in range(ntot-nlowP):
            if n>lowP+i*highPbin and n<=lowP+(i+1)*highPbin:
                return int(i+nlowP)
            
class Slot_machine:

    def __init__(self):

        self.symbols = [ 'lobster','bacon', 'turkey', 'cherry',  'salad','kebab', 'hotdog', 'icecream',  'pizza','whisky']
        self.jolly = 'lobster'
        self.major = ['turkey', 'bacon']
        self.minor = ['salad', 'cherry', 'kebab']
         
        self.n_columns = 5
        self.n_lines = 5
        self.emoji={'pizza': u'\U0001F355',
                    'lobster':u'\U0001F99E',
                    'kebab':u'\U0001F32F',
                    'hotdog':u'\U0001F32D',
                    'icecream':u'\U0001F366',
                    'cherry': u'\U0001F352',
                    'bacon': u'\U0001F953',
                    'turkey': u'\U0001F983',
                    'salad': u'\U0001F957',
                    'whisky': u'\U0001F943'                    
        }
        self.prizes={'jolly3': 80,
                     'jolly4': 800,
                     'jolly5': 50000,
                     'major3': 15,
                     'major4': 90,
                     'major5': 1500,
                     'minor3': 8,
                     'minor4': 30,
                     'minor5': 500
        }
    def run(self):
        self.columns=[]
        self.effprize=0
        self.effprizeMessage=''
        for c in range(self.n_columns):
            column = []
            for l in range(self.n_lines):
                index=nurandint()
                column.append(self.symbols[index])
            '''
            index=nurandint()
            if index+self.n_lines<10:
                for i in range(index,index+self.n_lines):
                    column.append(self.symbols[i])
            else:
                for i in range(index,10):
                    column.append(self.symbols[i])
                for i in range(0,(index+self.n_lines)%10):
                    column.append(self.symbols[i])
            '''
            self.columns.append(column)
        self.check()
        #print(self.counters)
        self.check_prize()

    def check(self):
        self.counters={
            'jolly_p' : [0, self.emoji[self.jolly]+' sulla diagonale principale'] ,
            'major_p' : [0, self.emoji[self.major[0]]+'/'+self.emoji[self.major[1]]+' sulla diagonale principale'],
            'minor_p' : [0, self.emoji[self.minor[0]]+'/'+self.emoji[self.minor[1]]+'/'+self.emoji[self.minor[2]]+' sulla diagonale principale'],
            'jolly_s' : [0, self.emoji[self.jolly]+' sulla diagonale secondaria'] ,
            'major_s' : [0, self.emoji[self.major[0]]+'/'+self.emoji[self.major[1]]+' sulla diagonale secondaria'],
            'minor_s' : [0, self.emoji[self.minor[0]]+'/'+self.emoji[self.minor[1]]+'/'+self.emoji[self.minor[2]]+' sulla diagonale secondaria'],
            'jolly_c' : [0, self.emoji[self.jolly]+' sulla riga centrale'] ,
            'major_c' : [0, self.emoji[self.major[0]]+'/'+self.emoji[self.major[1]]+' sulla riga centrale'],
            'minor_c' : [0, self.emoji[self.minor[0]]+'/'+self.emoji[self.minor[1]]+'/'+self.emoji[self.minor[2]]+' sulla riga centrale'],
        }
        for i in range(self.n_columns):
            for j in range(self.n_lines):
                if i==j:
                    self.check_symbol(i, j, '_p')
                if 4-i==j:
                    self.check_symbol(i, j, '_s')
                if j==2:
                    self.check_symbol(i, j, '_c')
                    
    def check_symbol(self, i, j, ext):
        if self.columns[i][j] == self.jolly:
            self.counters['jolly'+ext][0] += 1
        elif self.columns[i][j] in self.major:
            self.counters['major'+ext][0] += 1
        elif self.columns[i][j] in self.minor:
            self.counters['minor'+ext][0] += 1
    def check_prize(self):
        #print('giocata')

        for counter in self.counters:
            if self.counters[counter][0]>2:
                self.effprize+=self.prizes[str(counter[:5])+str(self.counters[counter][0])]
                self.effprizeMessage+='Sono usciti ' + str(self.counters[counter][0])+ ' '+ self.counters[counter][1]+'!\n'
                #print(self.effprize, counter, self.counters[counter])
from itertools import islice

def check_list(lst, x, n):
    gen = (True for i in lst if i==x)
    return next(islice(gen, n-1, None), False)
