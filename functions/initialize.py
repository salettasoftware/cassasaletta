# coding=utf-8%
from common_imports import *
def initialize():
    try:
        people=np.load('people.npy', allow_pickle = True).item()
        return 0
    except:
        if not os.path.isfile("./people.npy"):
            people={}
            people['CassaSaletta']={'username': 'CassaSaletta', 'nome':'CassaSaletta', 'id':0, 'saldo':217.10, 'paga':[]}
            np.save('people.npy', people)
            bot.sendMessage(-463787751, 'Vi do il benvenuto in Saletta.\n\nIo sono il bot delle casse del tesoro della saletta (i.e. funzionario del tesoro).\n\nPer entrare in questo magico mondo è necessario che digitiate il comando /start <saldo_iniziale> <NomeSaletta> per essere inizializzati a <saldo_iniziale> ed essere riconosciuti in futuro come <NomeSaletta>, anche dagli altri utenti. Dopo essere stati inizializzati con il comando /help avrete una breve descrizione degli ulteriori comandi disponibili e della loro sintassi di base.\n\nVi sconsiglio di provare a casaccio i vari comandi, perche il denaro vi verrà decurtato realmente.\n\nPer favore, scegliete un nome sensato e il valore reale del vostro saldo iniziale (in forma cartacea attualmente, chiedete a qualcuno in Saletta se non lo ricordate).\n\nBadate bene: IO posso correggere un vostro abuso; a voi non conviene invece farvi come nemico chi gestisce il vostro portafogli.\n\nBuono shopping :) .')
            return 0
