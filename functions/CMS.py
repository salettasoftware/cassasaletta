# coding=utf-8%
from common_imports import *

@checkRoles(min_role = 2)
def CMS(command, people, id_map):
    u"""/CMS <nome saletta>\n\nImposta a True la CMSitudine di <nome saletta>. Sono necessari privilegi particolari per usare questa funzione."""

    person = id_map[command.message.split()[1]]['id']
    try:
        person=id_map[command.message.split()[1]]['id']
    except:
        bot.sendMessage(command.chat_id, 'Errore, nome scorretto o non esistente.')
        return 1
    people[person]['CMS']=True
    bot.sendMessage(command.chat_id, people[person]['sname']+', sei stato riconosciuto come di CMS. Sopporta col grado di onore che ritieni opportuno le consegunze delle tue scelte dissennate.')
    return 0
