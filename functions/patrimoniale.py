# coding=utf-8%
from common_imports import *

@checkRoles(min_role = 2)
def patrimoniale(command, people, id_map):
    count=0
    scarto=26.55

    for person in people:
        if person!='CassaSaletta':
            
            print(people[person]['sname'])
            people[person]['saldo']-=scarto/(len(people)-1)
            count+=people[person]['saldo']
    people['CassaSaletta']['saldo']-=scarto
            
    print('saldo saletta: ',people['CassaSaletta']['saldo'])
    print('netto: ', people['CassaSaletta']['saldo']-count)
    bot.sendMessage(command.chat_id, "Udite udite, o villici della Saletta, \nCi rincresce constatare come qualche fellone abbia indebitamente usurpato il conio delle regali casse della Saletta. Il concilio ristretto, presieduto dal compagno presidente in carica sua maesta' il Saggio Limone, si e' pubblicamente riunito in data odierna attorno al tavolo rosso, ed ha cosi' deliberato:\nVisti:\n- L'ammanco nel forziere di denaro contante pari a 26.55 euri.\n- Le imminenti pubbliche celebrazioni del Santo Natale.\n- L'audacia nel rendere ignote le sue tracce del lestofante, il quale, qualora colto in flagranza dello misfatto, sarebbe stato giustiziato come nemico del popolo sul tavolo rosso durante le celebrazioni.\n- Lo pecorino montanaro.\n\nProclama: \nPrelievo coatto, forzioso e forzato (crepi l'avarizia) della pecunia di cui sopra, giustissimamente, equamente e democraticamente ripartita tra tutti i compagni della saletta, nessuno escluso.\n\nPossa l'onore tornare ad abitare questi luoghi, e la grandiosita' della Saletta risplendere fulgidissima nei secoli a venire.\nAmen.")
    #for person in people:
    #   if person!='CassaSaletta':
    bot.sendMessage(command.chat_id,"Villici, il vostro credito e' stato decurtato di: " + str(scarto/(len(people)-1))+ u'\u20AC.')

    bot.sendMessage(command.chat_id, "Il denaro nei forzieri e' pari a: " + str(people['CassaSaletta']['saldo'])+ u'\u20AC.')
    bot.sendMessage(command.chat_id, "Il netto della saletta e' pari a: "+ str(people['CassaSaletta']['saldo'] - count) + u'\u20AC.')

    return 0
