from common_imports import *
def ban(command, people, id_map):
    banned_user=id_map[command.message.split()[1]]['id']
    people[banned_user]['ban_score'][0]+=0.1
    people[banned_user]['ban_score'][1]=command.date
    bot.sendMessage(command.chat_id, people[banned_user]['sname']+" sei stato bannato/a. Pagherai il 10 percento in piu' rispetto al prezzo standard per una settimana. Peace.")
    return 0
