# coding=utf-8%
from common_imports import *

@checkRoles(min_role = 0)
def contatore(command, people, id_map):
    u"""/contatore\n\nComunica il numero di partite da quando qualcuno ha vinto qualcosa alla slot l'ultima volta. Induce fortemente a giocare."""
    bot.sendMessage(command.chat_id, "`Numero di giocate dall'ultima vittoria: " +str(people['CassaSaletta']['contatore_slot'])+'`', parse_mode='Markdown')
    return 0
