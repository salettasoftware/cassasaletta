# coding=utf-8%     
from common_imports import *

def check_ban(command, people, id_map):
    try:
        if people[command.fid]['ban_score'][0]!=1:
            if command.date.day-people[command.fid]['ban_score'][1].day>0:
                people[command.fid]['ban_score'][0]-=0.1
                bot.sendMessage(command.chat_id, 'Complimenti '+ people[command.fid]['sname']+u', ora sei stata de-bannata. Il prezzo di ciò che acquisterai torna al'+ str(people[command.fid]['ban_score'][0]*100)+'% del prezzo reale.')
    except:
        time.sleep(1)
