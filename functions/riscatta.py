from common_imports import *                    
from caffe import *
from sticker import *
from pasta import *

@checkRoles(min_role = 0)
def riscatta(command, people, id_map):
    u"""/riscatta <benefit> (<persona>)(<importo>)\n\nConsente di ottenere <benefit> consumando i propri punti. I <benefit> ottenibili sono(pt=costo in punti):\n\n'sticker'->10pt \nOttieni uno sticker di prima generazione unico ed inimitabile.\n\n'caffe'->5pt \nAutoesplicativo, un caffe offerto dalla saletta.\n\n'caffeSpeciale <persona>'->30pt\nUn caffe che verra' servito dal membro della Saletta che si desidera (E' necessario dunque specificare <persona>).\n\n'saltaLavaggio'->90pt\nAnche questo abbastanza autoesplicativo.\n\n'pasto <importo>'->15pt x euro\nUn pasto del valore di <importo> offerto dalla saletta.\n\n'viaggio'->50kpt \nUn viaggio all'Isola di Pasqua/Hawaii all inclusive della durata di due settimane\n\n'viaggioC'->25kpt \nUn viaggio all'Isola di Pasqua/Hawaii all inclusive della durata di due settimane con compagnia aerea cinese e scalo Wuhan.\n\n Nuove proposte possono essere aggiunte successivamente all'approvazione del presidente."""

    benefit=command.message.split()[1]
    current_points=people[command.fid]['punti']
    
    punti_caffe=5
    punti_sticker=10
    punti_pasto=45
    punti_caffeSpeciale=30
    punti_lavaggio=90
    punti_viaggio=50000
    punti_viaggioC=25000
    #################################################
    if benefit=='caffe':
        if current_points>=punti_caffe:
            caffe(command, people, id_map, riscatta_flag=1)
            punti=punti_caffe
            message='un caffe'
        else:
            bot.sendMessage(command.chat_id, 'Mi dispiace ' +people[command.fid]['sname']+', i tuoi punti ammontano a '+str(current_points)+", e non sono sufficienti per dell'ottimo caffe(punti caffe="+str(punti_caffe)+").")
            return 1

    elif benefit=='sticker':
        if current_points>=punti_sticker:
            sticker(command, people, id_map, riscatta_flag=1)
            punti=punti_sticker
            message='uno sticker unico ed inimitabile di prima generazione'
        else:
            bot.sendMessage(command.chat_id, 'Mi dispiace ' +people[command.fid]['sname']+', i tuoi punti ammontano a '+str(current_points)+", e non sono sufficienti per un fighissimo sticker (punti sticker="+str(punti_sticker)+").")
            return 1
        
    elif benefit=='pasto':
        cost=float(command.message.split()[2])
        punti=punti_pasto*cost/3.
        if current_points>=punti:
            temp_message = command.message
            command.set_message('/pasta '+str(cost))
            pasta(command, people, id_map, riscatta_flag=1)
            message='un pasto'
            command.set_message(temp_message)
        else:
            bot.sendMessage(command.chat_id, 'Mi dispiace ' +people[command.fid]['sname']+', i tuoi punti ammontano a '+str(current_points)+", e non sono sufficienti per un ottimo pasto (punti pasto="+str(punti)+").")
            return 1
    elif benefit=='caffeSpeciale':
        if current_points>=punti_caffeSpeciale:
            transporter=return_fid(command,people, id_map, command.message.split()[2]) 
            caffe(command, people, id_map, riscatta_flag=1)
            punti=punti_caffeSpeciale
            message='un caffe speciale portato da '+ people[transporter]['sname']
        else:
            bot.sendMessage(command.chat_id, 'Mi dispiace ' +people[command.fid]['sname']+', i tuoi punti ammontano a '+str(current_points)+", e non sono sufficienti per un caffe speciale (punti caffeSpeciale="+str(punti_caffeSpeciale)+").")
            return 1
    elif benefit=='viaggio':
        if current_points>=punti_viaggio:
            bot.sendMessage(command.chat_id, 'Complimenti ' +people[command.fid]['sname']+"! Hai riscattato un viaggio all'inclusive all'Isola di pasqua/Hawaii volando con una compagnia non cinese!")
            people[command.fid]['punti']-=punti_viaggio
            return 0
        else:
            bot.sendMessage(command.chat_id, 'Mi dispiace ' +people[command.fid]['sname']+', i tuoi punti ammontano a '+str(current_points)+", e non sono sufficienti per un super viaggio offerto dalla Saletta (punti viaggio="+str(punti_viaggio)+").")
            return 1
    elif benefit=='viaggioC':
        if current_points>=punti_viaggioC:
            bot.sendMessage(command.chat_id, 'Complimenti ' +people[command.fid]['sname']+"! Hai riscattato un viaggio all'inclusive all'Isola di pasqua/Hawaii volando con una compagni cinese e scalo a Wuhan!")
            people[command.fid]['punti']-=punti_viaggioC
            return 0
        else:
            bot.sendMessage(command.chat_id, 'Mi dispiace ' +people[command.fid]['sname']+', i tuoi punti ammontano a '+str(current_points)+", e non sono sufficienti per un super viaggio offerto dalla Saletta (punti viaggio="+str(punti_viaggioC)+").")
            return 1
    else:
        bot.sendMessage(command.chat_id, "C'e' qualche errore nella sintassi o la funzione non e' stata ancora implementata")
        return 1
    ###################################################################
    
    people[command.fid]['punti']-=punti
    bot.sendMessage(command.chat_id, "Ottimo "+people[command.fid]['sname']+', hai riscosso '+message+' spendendo '+str(punti)+' punti! I tuoi punti ammontano ora a: '+str(people[command.fid]['punti']))
    return 0
    
