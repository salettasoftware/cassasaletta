# coding=utf-8%
from common_imports import *

@checkRoles(min_role = 0)
def shame(command, people, id_map):
    u"""/shame\n\nMette in imbarazzo i debitori della saletta"""
    debtor=[]
    CMS=[]
    for person in people:
        if people[person]['saldo']<0:
            debtor.append(person)
            bot.sendMessage(command.chat_id, str(people[person]['sname'])+ u', il tuo credito è di ' +str(people[person]['saldo']) +u'\u20AC.')
    if len(debtor)!=0:
        bot.sendVideo(command.chat_id, 'https://media.giphy.com/media/vX9WcCiWwUF7G/giphy.gif')
        for d in debtor:
            if people[d]['saldo']<-15:
                bot.sendMessage(command.chat_id, 'Oh '+str(people[d]['sname'])+ u', che VERGOGNA!')
                bot.sendVideo(command.chat_id, 'https://media.giphy.com/media/Db3OfoegpwajK/giphy.gif')
    else:
        bot.sendMessage(command.chat_id, 'Benone, nessuno in negativo, bravi ragazzi!')
    for person in people:
        if person!='CassaSaletta':
            if people[person]['CMS']:
                bot.sendMessage(command.chat_id, str(people[person]['sname'])+ u', sei di CMS.')
                CMS.append(person)
    if len(CMS)!=0:
        bot.sendVideo(command.chat_id, 'https://media.giphy.com/media/88hFCyPGxSCze/giphy.gif')

    return 0
