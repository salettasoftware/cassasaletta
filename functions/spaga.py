from common_imports import *

@checkRoles(min_role = 0)
def spaga(command, people, id_map, disable_message=False):
    u"""/spaga <importo>\n\nAumenta il saldo personale di <importo>, senza toccare il saldo della cassa, a differenza di ricarica. E' possibile annullare la spesa solo se quest'ultima si trova fra le ultime dieci spese della giornata."""
    if check_nan(command, people, id_map, 1):
        return 1
    try:
        if float(command.message.split()[1])>=0:
            c=abs(float(command.message.split()[1]))
        else:
            bot.sendMessage(command.chat_id, 'Importi negativi non consentiti.')
            return 1
        
    except:
        if not disable_message:
            bot.sendMessage(command.chat_id, 'Importo selezionato non valido!\nDigita /spaga <importo> per rimediare ai tuoi errori!')
            return 1
    costo=people[command.fid]['ban_score'][0]*c
    spaga_flag=1
    for i in range(1, min(11, len(people[command.fid]['paga']))):
        if people[command.fid]['paga'][-i][1].day==command.date.day and people[command.fid]['paga'][-i][1].month==command.date.month:
            if people[command.fid]['paga'][-i][0]==costo:
                people[command.fid]['saldo']+=costo
                people[command.fid]['punti']-=c
                people[command.fid]['paga'].pop(-i)
                bot.sendMessage(command.chat_id, people[command.fid]['sname'] + ' ha annullato il pagamento di  '+str(costo)+u'\u20AC! Il saldo torna a: '+str(people[command.fid]['saldo'])+u'\u20AC.')
                spaga_flag=0
                return 0
            
    if spaga_flag:
        bot.sendMessage(command.chat_id, people[command.fid]['sname'] + ' fra le ultime dieci spese della giornata pare non ce ne sia nessuna che corrisponda a '+str(costo)+u"\u20AC. Stai provando a rubacchiare per caso? Il ban e' dietro l'angolo.")
        return 0
