# coding=utf-8%
from common_imports import *

@checkRoles(min_role = 3)
def update(command, people, id_map):

    people['718070237']['sname']='CapoDegliSgravoni'
    people['835357414']['sname']='Pimpi'
    bot.sendMessage(command.chat_id, "******UPDATE******\nCome deliberato durante l'ultimo meeting della Saletta, di seguito le news:\n\n--Aggiornamento del nome dello Sgravone da Padibis a -> CapoDegliSgravoni\n--Aggiornamento del nome di Pimpi da epp a -> Pimpi\n--Introduzione della nuovissima funzione /spaga <importo>, il cui effetto e' l'esatto opposto di /paga <importo>. Aumenta il saldo personale di <importo>, senza toccare il saldo della cassa, a differenza di /ricarica <importo>.\nIMPORTANTE: se vi sbagliate e pagate qualcosa per errore, per tornare indietro usate questa, non ricarica, altrimenti tocca fare altre patrimoniali.\nIMPORTANTE 2: serve ad annullare solo e solamente /paga, non caffe' o paste.\nANCORA PIU' IMPORTANTE: se adesso vi mettete a fare prove a casaccio, vi banno :) (con l'eccezione di Justino che deve vedere se funziona).") 

    return 0
