# coding=utf-8%
from common_imports import *

@checkRoles(min_role = 0)
def slot(command, people, id_map):
    u"""/slot\n\nGioca alla slot machine al costo di 30 centesimi. Si vince allineando 3/4/5 simboli vincenti sulla riga centrale, sulla diagonale principale, o sulla diagonale secondaria.\n\nPremi:\n3 x \U0001F957/\U0001F352/\U0001F32F: 8 punti\n4 x \U0001F957/\U0001F352/\U0001F32F: 30 punti\n5 x \U0001F957/\U0001F352/\U0001F32F: 500 punti\n\n3 x \U0001F983/\U0001F953+': 15 punti\n4 x \U0001F983/\U0001F953: 90 punti\n5 x \U0001F983/\U0001F953: 1500 punti\n\n3 x \U0001F99E: 80 punti\n4 x \U0001F99E: 800 punti\n5 x \U0001F99E: 50000 punti\n\n\nGiocate tanto.\nGiocate bene.\nGiocate forte.\n"""
    if people[command.fid]['saldo']<0.3:
        bot.sendMessage(command.chat_id, "Credito insufficiente, il costo di una partita e' 0.3 euro. La saletta non fa credito sul gioco d'azzardo")
        return 0
    people[command.fid]['saldo']-=0.3
    slot=Slot_machine()
    message=slot.run()
    emojis=[]
    emoji_message= u'`'
    format_mess_post = [[" \\ ", "   ", "   ", " / ", "/"],
                        [" \\ ", " \\ ", " / ", " / ", ""],
                        ["   ", " | ", " | ", "   ", ""],
                        [" / ", " / ", " \\ ", " \\ ", ""],
                        [" / ", "   ", "   ", " \\ ", "\\"]
    ]
    format_mess_pre = [["\\", "", "", "", ""],
                       [" ", "", "", "", ""],
                       [" ", "", "", "", ""],
                       [" ", "", "", "", ""],
                       ["/", "", "", "", ""]
    ]  
    for i,column in enumerate(slot.columns):
        for j,symbol in enumerate(column):
            emoji_message+=format_mess_pre[i][j]+slot.emoji[slot.columns[j][i]]+format_mess_post[i][j]
        emoji_message+='\n\n'

    emoji_message+='`'
    bot.sendMessage(command.chat_id, emoji_message, parse_mode='Markdown')
    print(emoji_message)
    if slot.effprize==0:
        people['CassaSaletta']['contatore_slot']+=1
        message='`Oh no, '+people[command.fid]['sname']+" non hai vinto! Numero di giocate dall'ultima vittoria: " +str(people['CassaSaletta']['contatore_slot'])+'`'        
    else:
        message = '`Complimenti '+people[command.fid]['sname']+', hai vinto '+str(slot.effprize)+' punti!\n' 
        message += slot.effprizeMessage
        message +='`'
        people['CassaSaletta']['contatore_slot']=0
    with open("logbook.txt" , "a+") as log:
        log.write(str(command.date) + " " + command.fid + " " + "win: " + str(slot.effprize) + "\n")
    people['CassaSaletta']['slotWins']+=slot.effprize
    people['CassaSaletta']['slotTries']+=1
    bot.sendMessage(command.chat_id, message, parse_mode='Markdown')
    people[command.fid]['punti'] += slot.effprize
    
    return 0 
