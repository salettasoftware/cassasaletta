# coding=utf-8
import sys
import time
import telepot
import unicodedata
import numpy as np
import datetime
from telepot.loop import MessageLoop
from pprint import pprint
sys.path.append('./functions')
from telepot.namedtuple import ReplyKeyboardMarkup, KeyboardButton
from command import *
from functions import *

TOKEN='631352319:AAGXSJaQD8HO7iI18sdV3MgIaA7VqELLpcA'
bot = telepot.Bot(TOKEN)

#TOKEN='1042986801:AAEW5sHcPOZVWUMHo4iOUsrFlymI71oSJmQ'
#bot = telepot.Bot(TOKEN)

def handle(msg):
    pprint(msg)
    
    content_type, chat_type, chat_id = telepot.glance(msg)
    
    print(content_type, chat_type, chat_id)
    try:
        people=np.load('people.npy', allow_pickle = True).item()
        
    except:
        return 0
    try:
        id_map=np.load('id_map.npy', allow_pickle = True).item()
    except:
        
        return 0
    if content_type == 'text':
        fname=msg["from"]["first_name"]
        fid=str(msg["from"]["id"])
        name=unicodedata.normalize('NFKD', fname).encode('ascii','ignore')
        message=msg['text']

        #lemon hack
        #if fid=="650624019": #'isaia_pandoori'
        #    bot.sendMessage(chat_id, 'Manco te rispondo.')
        #    return 0

        if '/start' not in message:
            if fid not in people:
                  bot.sendMessage(chat_id, 'Non sei nella lista delle persone autorizzate ad effettuare operazioni. Usa prima il comando /start <importo> <NomeSaletta>.')
                  return 0

        if fid in people:
            sname=people[fid]['sname']
        else:
            sname=''
        date=datetime.datetime.now()            
        Command=command(message, fid, name, chat_id, date, sname) 
        succes_code = not(Command.execute(people, id_map))
        if succes_code:
            with open("logbook.txt", "a+") as log:
                #log.write(str(date)+" "+ Command.fid+" " + str(Command.message.encode("utf-8"))+"\n")
                log.write(str(date)+" "+ Command.fid+" " + str(Command.message)+"\n")
        np.save('people.npy', people)    

MessageLoop(bot, handle).run_as_thread()
initialize.initialize()
print ('Listening ...')

# Keep the program running.
while 1:
    time.sleep(10)

