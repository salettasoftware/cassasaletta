import slot_machine
slot=Slot()
r=1000000.
n=4
cost=0.3
c_j=0.
c_M=0.
c_m=0.
money=0
countWins={
            'jolly3' : 0,
            'major3' : 0,
            'minor3' : 0,
            'jolly4' : 0,
            'major4' : 0,
            'minor4' : 0,
            'jolly5' : 0,
            'major5' : 0,
            'minor5' : 0,
        }
conf_tot_win=0
conf_tot_lose=0
for i in range(int(r)):
    if i%100000==0:
        print('Siamo al '+str(i*100/r)+'%')
    slot.run()
    money+=(cost*15)
    conf_tot_lose+=(cost*15)
    money-=slot.effprize
    conf_tot_win+=slot.effprize
    #print(money)
    for counter in slot.counters:
        if slot.counters[counter][0]>2:
            #print(str(counter[:5])+'_'+str(slot.counters[counter][0]))
            countWins[str(counter[:5])+str(slot.counters[counter][0])]+=1
somma=0
totale_vinto=0
for counter in countWins:
    totale_vinto+=slot.prizes[counter]*countWins[counter]
    countWins[counter]/=r
    somma+=countWins[counter]
    try:
        prob_equa=(cost*15)/(9*countWins[counter])
    except:
        prob_equa=0
    print('percentuale di '+counter+' '+str(countWins[counter])+'. Vincita equa: '+str(prob_equa)+'. Vincita totale del premio: '+str(countWins[counter]*r*slot.prizes[counter]))

print('saldo saletta finale:' , money/15., ',  guadagno relativo:', (money/15.)/(r*cost))
print('prob di vincere qualcosa', somma)
print('totale vinto: ', totale_vinto, 'totale speso: ', r*cost*15, 'confronto: ', conf_tot_win, ' e: ', conf_tot_lose)
